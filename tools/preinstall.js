const fs = require('fs');
const path = require('path');
const exec = require('child_process').exec;

console.log('preinstall script running...');

const tslintConfig = 'tslint.json';

getAppRootFolder().then(appRootFolder =>
  copyConfig(tslintConfig, appRootFolder)
).then(appRootFolder => {
  console.log("appRootFolder 2: " + appRootFolder);
  console.log("RUNNING MKDIR ...");
  fs.mkdirSync(path.join(appRootFolder, 'app'))
  console.log("RUNNING MKDIR SUCESS");
});

function copyConfig(configFilename, appRootFolder) {
  return new Promise((resolve, reject) => {
    console.log("appRootFolder: " + appRootFolder);
    const sourcePath = path.join(__dirname, configFilename);
    const destPath = path.join(appRootFolder, configFilename);
    console.log("sourcePath: " + sourcePath);
    console.log("destPath: " + destPath);

    console.log(`creating ${path.resolve(destPath)}...`);
    fs.rename(sourcePath, destPath, err => {
      if (err) {
        console.log("ERROR: " + err);
        return reject(err);
      }
      console.log("RENAME SUCCESS");
      resolve(appRootFolder);
    });
  });
}

function getAppRootFolder() {
  return new Promise((resolve, reject) => {
      console.log("RUNNING MKDIR...");
    // npm prefix returns the closest parent directory to contain a package.json file
    exec('cd .. && npm prefix', (err, stdout) => {
        console.log("RUNNING MKDIR..."+stdout);
      if (err) {
        console.log("ERROR: " + err);
        return reject(err);
      }
      resolve(stdout.toString().trim());
    });
  });
}
